use serde::Deserialize;
use serde::Serialize;
use std::env;
use std::fs;
use std::ops::Index;
use upm::UpmErr;

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone, Copy)]
enum TaskState {
    BackBurner,
    Todo,
    Doing,
    AwaitingChanges,
    Finished,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct Task {
    name: String,
    state: TaskState,
    notes: Vec<String>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
struct Kanban {
    tasks: Vec<Task>,
}

fn load(file: &str) -> Result<Kanban, std::io::Error> {
    let file = fs::read_to_string(file)?;
    let kbn: Kanban = serde_yaml::from_str(file.as_str()).unwrap();
    Ok(kbn)
}

fn save(kbn: Kanban, filename: &str) -> Result<(), serde_yaml::Error> {
    let yaml = serde_yaml::to_string(&kbn)?;
    fs::write(filename, yaml).unwrap();
    Ok(())
}

fn print_kanban(kbn: &Kanban) {
    if kbn.tasks.len() == 0 {
        return;
    }
    let mut len = 15;
    for t in &kbn.tasks {
        if len < t.name.len() {
            len = t.name.len();
        }
    }
    len = len + 1;
    println!(
        "┌──┬{:─<len$}┬{:─<len$}┬{:─<len$}┬{:─<len$}┬{:─<len$}┐",
        "", "", "", "", ""
    );
    println!(
        "│  │{: ^len$}│{: ^len$}│{: ^len$}│{: ^len$}│{: ^len$}│",
        "Back Burner", "Todo", "Doing", "Awaiting Changes", "Finished"
    );
    println!(
        "├──┼{:─<len$}┼{:─<len$}┼{:─<len$}┼{:─<len$}┼{:─<len$}┤",
        "", "", "", "", ""
    );
    let mut i = 0;
    let mut active = &kbn.tasks[0];
    for t in &kbn.tasks {
        match &t.state {
            TaskState::BackBurner => println!(
                "│{: <2}│{: <len$}│{: <len$}│{: <len$}│{: <len$}│{: <len$}│",
                i, t.name, "", "", "", ""
            ),
            TaskState::Todo => println!(
                "│{: <2}│{: <len$}│{: <len$}│{: <len$}│{: <len$}│{: <len$}│",
                i, "", t.name, "", "", ""
            ),
            TaskState::Doing => println!(
                "│{: <2}│{: <len$}│{: <len$}│{: <len$}│{: <len$}│{: <len$}│",
                i, "", "", t.name, "", ""
            ),
            TaskState::AwaitingChanges => println!(
                "│{: <2}│{: <len$}│{: <len$}│{: <len$}│{: <len$}│{: <len$}│",
                i, "", "", "", t.name, ""
            ),
            TaskState::Finished => println!(
                "│{: <2}│{: <len$}│{: <len$}│{: <len$}│{: <len$}│{: <len$}│",
                i, "", "", "", "", t.name
            ),
        }
        i = i + 1;
        if t.state == TaskState::Doing {
            active = &t;
        }
    }
    println!(
        "└──┴{:─<len$}┴{:─<len$}┴{:─<len$}┴{:─<len$}┴{:─<len$}┘",
        "", "", "", "", ""
    );
    if active.state == TaskState::Doing {
        if active.notes.len() != 0 {
            println!("Notes for task {}", active.name);
        }
        let mut i = 0;
        for note in &active.notes {
            println!("{}. {}", i, note);
            i = i + 1;
        }
    }
}

fn kanban_add(kbn: &mut Kanban, task: String) {
    if task == "" {
        return;
    }
    kbn.tasks.push({
        Task {
            name: task,
            state: crate::TaskState::BackBurner,
            notes: vec![],
        }
    })
}

fn find_task_by_name_or_id(kbn: &mut Kanban, name_or_id: String) -> Result<usize, ()> {
    if let Ok(i) = usize::from_str_radix(name_or_id.as_str(), 10) {
        return Ok(i);
    } else {
        match find_task_by_name(kbn, name_or_id.clone(), false) {
            Ok(task) => {
                return Ok(task);
            }
            Err(_) => match find_task_by_name(kbn, name_or_id.clone(), true) {
                Ok(task) => {
                    return Ok(task);
                }
                Err(UpmErr::Ambiguous) => {
                    eprintln!("Ambiguous task name {}", &name_or_id);
                    return Err(());
                }
                Err(UpmErr::NotFound) => {
                    eprintln!("Could not find task {}", &name_or_id);
                    return Err(());
                }
                Err(_) => {
                    return Err(());
                }
            },
        }
    }
}

fn find_task_by_name(kbn: &mut Kanban, str: String, case_sensitive: bool) -> Result<usize, UpmErr> {
    let mut i: usize = 0;
    let mut tasks_upm = upm::Upm::new();
    for task in &kbn.tasks {
        if case_sensitive {
            tasks_upm.insert(task.name.as_str(), i)?;
        } else {
            tasks_upm.insert(task.name.to_lowercase().as_str(), i)?;
        }
        i = i + 1;
    }
    let searched = if case_sensitive {
        str
    } else {
        str.to_lowercase()
    };

    match tasks_upm.find(searched.as_str()) {
        Ok(task) => {
            return Ok(*task);
        }
        Err(e) => {
            return Err(e);
        }
    }
}

fn kanban_change_state(kbn: &mut Kanban, task: Option<String>, state: TaskState) -> Result<(), ()> {
    let mut changed_active = false;
    if let Some(str) = task {
        // user wants to make some task active, if other active task exists, move it to todo
        if state == TaskState::Doing {
            for task in &mut kbn.tasks {
                if task.state == TaskState::Doing {
                    task.state = TaskState::Todo;
                }
            }
        }

        let task_id = find_task_by_name_or_id(kbn, str)?;
        if kbn.tasks[task_id].state == TaskState::Doing {
            changed_active = true;
        }
        kbn.tasks[task_id].state = state;
    } else {
        eprintln!("Missing task number");
        return Err(());
    }
    if (state == TaskState::AwaitingChanges || state == TaskState::Finished) && changed_active {
        for task in &mut kbn.tasks {
            if task.state == TaskState::Todo {
                task.state = TaskState::Doing;
                return Ok(());
            }
        }
    }
    return Ok(());
}

fn kanban_remove_finished(kbn: &mut Kanban) -> Result<(), ()> {
    let mut i: usize = 0;
    loop {
        if kbn.tasks[i].state == TaskState::Finished {
            kbn.tasks.remove(i);
        } else {
            i = i + 1;
        }
        if i >= kbn.tasks.len() {
            return Ok(());
        }
    }
}

fn print_note(kbn: &mut Kanban, args: &mut std::env::Args) -> Result<(), ()> {
    if let Some(task) = args.next() {
        let num = find_task_by_name_or_id(kbn, task)?;

        if let Some(to_print) = kbn.tasks.get(num) {
            println!("Notes for task {}:", to_print.name);
            for note in &to_print.notes {
                println!("{}", note);
            }
            return Ok(());
        } else {
            eprintln!("Invalid index: {}", num);
            return Err(());
        }
    } else {
        let mut active = None;
        for t in &mut kbn.tasks {
            if t.state == TaskState::Doing {
                active = Some(t);
            }
        }
        if let Some(act) = active {
            println!("Notes for task {}:", act.name);
            for note in &act.notes {
                println!("{}", note);
            }
        }
    }
    Ok(())
}

fn kanban_process_note(kbn: &mut Kanban, args: &mut std::env::Args) -> Result<(), ()> {
    let mut active = None;
    for t in &mut kbn.tasks {
        if t.state == TaskState::Doing {
            active = Some(t);
        }
    }

    enum Action {
        Add,
        Remove,
        Show,
        Insert,
    }
    let mut upm = upm::Upm::new();

    upm.insert("remove", Action::Remove).map_err(|_| ())?;
    upm.insert("add", Action::Add).map_err(|_| ())?;
    upm.insert("delete", Action::Remove).map_err(|_| ())?;
    upm.insert("show", Action::Show).map_err(|_| ())?;
    upm.insert("insert", Action::Insert).map_err(|_| ())?;

    match args.next() {
        None => {
            return Ok(());
        }
        Some(s) => match upm.find(&s) {
            Ok(Action::Add) => {
                if let Some(act) = active {
                    let note = args.map(|x| format!("{} ", x)).collect::<String>();
                    act.notes.push(note);
                    return Ok(());
                } else {
                    eprintln!("No active task");
                    return Err(());
                }
            }
            Ok(Action::Remove) => {
                if let Some(act) = active {
                    let num = usize::from_str_radix(args.next().unwrap().as_str(), 10).unwrap();
                    act.notes.remove(num);
                    return Ok(());
                } else {
                    eprintln!("No active task");
                    return Err(());
                }
            }
            Ok(Action::Show) => {
                print_note(kbn, args)?;
            }
            Ok(Action::Insert) => {
                // Insert note to other than active task
                // TODO in show
                todo!();
            }
            Err(_) => {
                return Err(());
            }
        },
    }
    return Err(());
}

fn main() -> Result<(), UpmErr> {
    #[derive(Debug)]
    enum Action {
        Purge,
        Add,
        ChangeState(TaskState),
        Note,
    }
    let mut upm = upm::Upm::new();
    upm.insert("purge", Action::Purge)?;
    upm.insert("backburner", Action::ChangeState(TaskState::BackBurner))?;
    upm.insert("todo", Action::ChangeState(TaskState::Todo))?;
    upm.insert("doing", Action::ChangeState(TaskState::Doing))?;
    upm.insert("add", Action::Add)?;
    upm.insert("waiting", Action::ChangeState(TaskState::AwaitingChanges))?;
    upm.insert("finished", Action::ChangeState(TaskState::Finished))?;
    upm.insert("note", Action::Note)?;

    // in debug build use "./kbn.yaml"
    let kanban_dir: String = if cfg!(debug_assertions) {
        String::from("./kbn.yaml")
    } else {
        format!("{}/Sync/kanban.yaml", env::var("HOME").unwrap())
    };

    //try to load tasks from file
    let mut kbn = if let Ok(kbn) = load(kanban_dir.as_str()) {
        kbn
    } else {
        //file does not exists/is corrupted
        Kanban { tasks: vec![] }
    };

    let mut args = std::env::args();
    if args.len() > 1 {
        args.next();
        let command = &args.next().unwrap();
        let result = match upm.find(command) {
            Ok(action) => match action {
                Action::Purge => kanban_remove_finished(&mut kbn),
                Action::Note => kanban_process_note(&mut kbn, &mut args),
                Action::Add => {
                    kanban_add(
                        &mut kbn,
                        args.map(|x| format!("{} ", x)).collect::<String>(),
                    );
                    Ok(())
                }
                Action::ChangeState(new_state) => {
                    let remaining_args: String = args
                        .map(|x| format!("{} ", x))
                        .collect::<String>()
                        .trim()
                        .to_string();
                    kanban_change_state(&mut kbn, Some(remaining_args), *new_state)
                }
            },
            Err(UpmErr::Ambiguous) => {
                eprintln!("Ambiguous command \"{}\"", command);
                std::process::exit(1);
            }
            Err(UpmErr::NotFound) => {
                eprintln!("Unknown command \"{}\"", command);
                std::process::exit(1);
            }
            Err(UpmErr::Other(str)) => {
                eprintln!("Unknown error (probably a bug): {}", str);
                std::process::exit(1);
            }
            Err(UpmErr::AlereadyInserted) => {
                std::process::exit(1);
            }
        };
        if let Err(_) = result {
            return Ok(());
        }
    }
    print_kanban(&kbn);
    save(kbn, kanban_dir.as_str()).unwrap();
    Ok(())
}
