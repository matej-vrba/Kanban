use std::collections::BTreeMap;

#[derive(Debug)]
pub struct Upm<T> {
    sub_nodes: BTreeMap<char, Upm<T>>,
    key: String,
    val: Option<T>,
}

#[derive(Debug)]
pub enum UpmErr{
    Ambiguous,
    NotFound,
    AlereadyInserted,
    Other(String),
}

impl<T> Upm<T> {
    pub fn from(key: &str, val: T) -> Self {
        Self {
            sub_nodes: BTreeMap::new(),
            val: Some(val),
            key: key.into(),
        }
    }
    pub fn new() -> Self {
        Self {
            sub_nodes: BTreeMap::new(),
            val: None,
            key: "".into(),
        }
    }
    pub fn insert(&mut self, key: &str, val: T) -> Result<(), UpmErr> {
        if(self.find(key)).is_ok(){
            return Err(UpmErr::AlereadyInserted);
        }
        let next_ch = key.chars().next().unwrap();
        if self.val.is_some() {
            self.sub_nodes
                .entry(next_ch)
                .or_insert(Self::from(&key[1..], val));
            let next_ch = self.key.chars().next().unwrap();

            let self_val = std::mem::replace(&mut self.val, None);
            if let Some(self_val) = self_val {
                match self.sub_nodes.entry(next_ch) {
                    std::collections::btree_map::Entry::Vacant(e) => {
                        e.insert(Self::from(&self.key[1..], self_val));
                    }
                    std::collections::btree_map::Entry::Occupied(mut e) => {
                        e.get_mut().insert(&self.key[1..], self_val);
                    }
                }
            }
            self.val = None;
            self.key.clear();

            return Ok(());
        } else {
            match self.sub_nodes.get_mut(&next_ch) {
                None => {
                    self.sub_nodes
                        .entry(next_ch)
                        .or_insert(Self::from(&key[1..], val));
                    return Ok(());
                }
                Some(n) => {
                    return n.insert(&key[1..], val);
                }
            }
        }
    }

    pub fn find(&self, key: &str) -> Result<&T, UpmErr> {
        if self.key.starts_with(key) && self.sub_nodes.is_empty() {
            if let Some(val) = &self.val {
                return Ok(&val);
            } else {
                return Err(UpmErr::Other(format!("Key {} found, but had not value associated", key)));
            }
        } else {
            match key.chars().next() {
                Some(next_ch) => match self.sub_nodes.get(&next_ch) {
                    None => {
                        return Err(UpmErr::NotFound);
                    }
                    Some(e) => {
                        return e.find(&key[1..]);
                    }
                },
                None => {
                        return Err(UpmErr::Ambiguous);
                }
            }
        }
    }
}
